"""Module with service functions"""
import json
import os
import pickle
import yaml

from src.settings import ROOT_DIR


def read_yaml(file_path):
    """
    Read yaml file from disk
    :param file_path: str, path to file, including file name
    :return: dict
    """
    if not os.path.isabs(file_path):
        file_path = os.path.join(ROOT_DIR, file_path)
    with open(file_path, "r", encoding="utf-8") as file:
        return yaml.safe_load(file)


def read_json(file_path):
    """
    Read json file from disk
    :param file_path: str, path to file, including file name
    :return: dict
    """
    if not os.path.isabs(file_path):
        file_path = os.path.join(ROOT_DIR, file_path)
    with open(file_path, "r", encoding="utf-8") as file:
        return json.load(file)


def write_json(file_path, data):
    """
    Write data to disk
    :param file_path: str, path to file, including file name
    :param data: data for writing
    :return: None
    """
    if not os.path.isabs(file_path):
        file_path = os.path.join(ROOT_DIR, file_path)
    with open(file_path, "w", encoding="utf-8") as write_file:
        json.dump(data, write_file)


def save_pickle(file_path, obj):
    """
    Save obj to disk
    :param file_path: str, path to file, including file name
    :param obj: data for saving
    :return: None
    """
    if not os.path.isabs(file_path):
        file_path = os.path.join(ROOT_DIR, file_path)
    with open(file_path, "wb") as file:
        pickle.dump(obj, file)


def load_pickle(file_path):
    """
    Load pickle file from disk
    :param file_path: str, path to file, including file name
    :return: same object, saved as pickle before
    """
    if not os.path.isabs(file_path):
        file_path = os.path.join(ROOT_DIR, file_path)
    with open(file_path, "rb") as file:
        return pickle.load(file)
