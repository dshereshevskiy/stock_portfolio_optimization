# -*- coding: utf-8 -*-
import click
import logging
import pandas as pd
from src.features.build_features import add_features, fill_nan_inf


@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_filepath", type=click.Path())
def data_preprocessing(input_filepath, output_filepath):
    """
    Runs data processing scripts to turn raw data from (../raw) into
    cleaned data ready to be analyzed (saved in ../processed).

    :param input_filepath: str or click.Path, path to input raw data of csv format
    :param output_filepath: str or click.Path, path to output preprocessed data
    :return: pd.DataFrame, preprocessed data. This is need for standalone use of this function
    """
    logger = logging.getLogger(__name__)
    logger.info("making final data set from raw data")

    train = pd.read_csv(input_filepath, parse_dates=["Date"])
    train = (
        train.drop(
            columns=["RowId", "ExpectedDividend", "AdjustmentFactor", "SupervisionFlag"]
        )
        .dropna()
        .reset_index(drop=True)
    )
    train = add_features(train)
    train = fill_nan_inf(train)

    train.to_csv(output_filepath)

    return train


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    data_preprocessing()
