"""feature build code"""
import numpy as np


def add_features(feats):
    """
    Adding new features to dataset

    :param feats: pd.DataFrame, init data features
    :return: pd.DataFrame, data with added new features
    """
    feats["return_1month"] = feats["Close"].pct_change(20)
    feats["return_2month"] = feats["Close"].pct_change(40)
    feats["return_3month"] = feats["Close"].pct_change(60)
    feats["volatility_1month"] = np.log(feats["Close"]).diff().rolling(20).std()
    feats["volatility_2month"] = np.log(feats["Close"]).diff().rolling(40).std()
    feats["volatility_3month"] = np.log(feats["Close"]).diff().rolling(60).std()
    feats["MA_gap_1month"] = feats["Close"] / (feats["Close"].rolling(20).mean())
    feats["MA_gap_2month"] = feats["Close"] / (feats["Close"].rolling(40).mean())
    feats["MA_gap_3month"] = feats["Close"] / (feats["Close"].rolling(60).mean())

    return feats


def fill_nan_inf(df):
    """
    Fill nan and inf in dataframe
    :param df: pd.DataFrame, input data
    :return: pd.DataFrame, output filled data
    """
    df = df.fillna(0)
    df = df.replace([np.inf, -np.inf], 0)
    return df
