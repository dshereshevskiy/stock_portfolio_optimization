"""train module with mlflow tracking"""
import json
import logging
import os

import click
import dotenv
import mlflow
import numpy as np
import pandas as pd
import lightgbm as lgb
from mlflow.models import infer_signature
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score

from src.models.utils import feval_pearsonr
from src.utils.services import read_yaml
from src.settings import PROJECT_DIR


dotenv_path = os.path.join(PROJECT_DIR, '.env')
dotenv.load_dotenv(dotenv_path)

config = read_yaml("config.yaml")
TRAIN_DATA_FILEPATH = os.path.join(PROJECT_DIR, config["TRAIN_DATA_FILEPATH"])
TEST_DATA_FILEPATH = os.path.join(PROJECT_DIR, config["TEST_DATA_FILEPATH"])

# mlflow.set_tracking_uri("http://127.0.0.1:5000")

remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(remote_server_uri)
os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://158.160.106.86:9000"
mlflow.set_experiment('stock_portfolio_optimization')  # name of experiment
mlflow.lightgbm.autolog()


@click.command()
@click.option("--train_data_filepath", type=click.Path(exists=True), default=TRAIN_DATA_FILEPATH)
@click.option("--test_data_filepath", type=click.Path(exists=True), default=TEST_DATA_FILEPATH)
@click.option("--model_filepath", type=click.Path(), default=None)
@click.option("--score_filepath", type=click.Path(), default=None)
def train_model(train_data_filepath, test_data_filepath, model_filepath, score_filepath):
    """
    Train model and save it to the disk

    :param train_data_filepath: str or click.Path, path to train data
    :param test_data_filepath: str or click.Path, path to test data
    :param model_filepath: str or click.Path, path to save model
    :param score_filepath: str or click.Path, path to save scores
    :return: None
    """

    logger = logging.getLogger(__name__)
    logger.info("start training of the model...")

    train = pd.read_csv(train_data_filepath, parse_dates=["Date"])

    features = config["features"]
    params_lgb = config["params_lgb"]

    # data for train and validation
    list_spred_h = list(
        (
            train.groupby("SecuritiesCode")["Target"].max()
            - train.groupby("SecuritiesCode")["Target"].min()
        )
        .sort_values()[:1000]
        .index
    )
    list_spred_l = list(
        (
            train.groupby("SecuritiesCode")["Target"].max()
            - train.groupby("SecuritiesCode")["Target"].min()
        )
        .sort_values()[1000:]
        .index
    )

    X_train = train[train["SecuritiesCode"].isin(list_spred_h)][features].astype(float)
    y_train = train[train["SecuritiesCode"].isin(list_spred_h)]["Target"].astype(float)
    tr_dataset = lgb.Dataset(
        X_train,
        y_train,
        feature_name=features,
    )
    X_valid = train[train["SecuritiesCode"].isin(list_spred_l)][features].astype(float)
    y_valid = train[train["SecuritiesCode"].isin(list_spred_l)]["Target"].astype(float)
    vl_dataset = lgb.Dataset(
        X_valid,
        y_valid,
        feature_name=features,
    )
    # create and train models
    model = lgb.train(
        params=params_lgb,
        train_set=tr_dataset,
        valid_sets=[tr_dataset, vl_dataset],
        num_boost_round=3000,
        feval=feval_pearsonr,
        callbacks=[
            lgb.early_stopping(stopping_rounds=300, verbose=True),
            lgb.log_evaluation(period=100),
        ],
    )

    # Use the model to make predictions on the test dataset.
    test = pd.read_csv(test_data_filepath, parse_dates=["Date"])
    X_test = test[features].astype(float)
    y_test = test.Target.astype(float)
    predictions = model.predict(X_test, num_iteration=model.best_iteration)

    score = dict(
        rmse=np.sqrt(mean_squared_error(y_test, predictions)),
        mae=mean_absolute_error(y_test, predictions),
        r2=r2_score(y_test, predictions)
    )

    # logging by mlflow - params, scores and model
    signature = infer_signature(X_test, predictions)

    for k, v in params_lgb.items():
        mlflow.log_param(k, v)
    for k, v in score.items():
        mlflow.log_metric(k, v)
    mlflow.lightgbm.log_model(lgb_model=model,
                              artifact_path="stock_portfolio_optimization",
                              signature=signature)

    # save model and score
    if model_filepath is not None:
        model.save_model(model_filepath)
    if score_filepath is not None:
        with open(score_filepath, "w") as score_file:
            json.dump(score, score_file)


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    train_model()
