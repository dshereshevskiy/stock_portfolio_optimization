import numpy as np
import pandas as pd
from scipy import stats
from sklearn.metrics import mean_squared_error


def feval_rmse(y_pred, lgb_train):
    y_true = lgb_train.get_label()
    return 'rmse', mean_squared_error(y_true, y_pred), False


def feval_pearsonr(y_pred, lgb_train):
    y_true = lgb_train.get_label()
    return 'pearsonr', stats.pearsonr(y_true, y_pred)[0], True


def calc_spread_return_per_day(df, portfolio_size=200, toprank_weight_ratio=2):
    assert df['Rank'].min() == 0
    assert df['Rank'].max() == len(df['Rank']) - 1
    weights = np.linspace(start=toprank_weight_ratio, stop=1, num=portfolio_size)
    purchase = (df.sort_values(by='Rank')['Target'][:portfolio_size] * weights).sum() / weights.mean()
    short = (df.sort_values(by='Rank', ascending=False)['Target'][:portfolio_size] * weights).sum() / weights.mean()
    return purchase - short


def calc_spread_return_sharpe(df: pd.DataFrame, portfolio_size=200, toprank_weight_ratio=2):
    buf = df.groupby('Date').apply(calc_spread_return_per_day, portfolio_size, toprank_weight_ratio)
    sharpe_ratio = buf.mean() / buf.std()
    return sharpe_ratio  # , buf


def add_rank(df):
    df["Rank"] = df.groupby("Date")["Target"].rank(ascending=False, method="first") - 1
    df["Rank"] = df["Rank"].astype("int")
    return df


def fill_nan_inf(df):
    df = df.fillna(0)
    df = df.replace([np.inf, -np.inf], 0)
    return df


def check_score(df, preds, Securities_filter=()):
    tmp_preds = df[['Date', 'SecuritiesCode']].copy()
    tmp_preds['Target'] = preds

    # Rank Filter. Calculate median for this date and assign this value to the list of Securities to filter.
    tmp_preds['target_mean'] = tmp_preds.groupby("Date")["Target"].transform('median')
    tmp_preds.loc[tmp_preds['SecuritiesCode'].isin(Securities_filter), 'Target'] = tmp_preds['target_mean']

    tmp_preds = add_rank(tmp_preds)
    df['Rank'] = tmp_preds['Rank']
    score = round(calc_spread_return_sharpe(df, portfolio_size=200, toprank_weight_ratio=2), 5)
    score_mean = round(df.groupby('Date').apply(calc_spread_return_per_day, 200, 2).mean(), 5)
    score_std = round(df.groupby('Date').apply(calc_spread_return_per_day, 200, 2).std(), 5)
    print(f'Competition_Score:{score}, rank_score_mean:{score_mean}, rank_score_std:{score_std}')
