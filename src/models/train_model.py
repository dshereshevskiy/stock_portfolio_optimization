"""train module"""
import logging

import click
import pandas as pd
import lightgbm as lgb

from src.utils.services import read_yaml
from src.models.utils import feval_pearsonr


@click.command()
@click.argument("train_data_filepath", type=click.Path(exists=True))
@click.argument("model_filepath", type=click.Path())
def train_model(train_data_filepath, model_filepath):
    """
    Train model and save it to the disk

    :param train_data_filepath: str or click.Path, path to train data
    :param model_filepath: str or click.Path, path to save model
    :return: None
    """

    logger = logging.getLogger(__name__)
    logger.info("start training of the model...")

    config = read_yaml("config.yaml")

    train = pd.read_csv(train_data_filepath, parse_dates=["Date"])

    features = config["features"]
    params_lgb = config["params_lgb"]

    list_spred_h = list(
        (
            train.groupby("SecuritiesCode")["Target"].max()
            - train.groupby("SecuritiesCode")["Target"].min()
        )
        .sort_values()[:1000]
        .index
    )
    list_spred_l = list(
        (
            train.groupby("SecuritiesCode")["Target"].max()
            - train.groupby("SecuritiesCode")["Target"].min()
        )
        .sort_values()[1000:]
        .index
    )

    tr_dataset = lgb.Dataset(
        train[train["SecuritiesCode"].isin(list_spred_h)][features],
        train[train["SecuritiesCode"].isin(list_spred_h)]["Target"],
        feature_name=features,
    )
    vl_dataset = lgb.Dataset(
        train[train["SecuritiesCode"].isin(list_spred_l)][features],
        train[train["SecuritiesCode"].isin(list_spred_l)]["Target"],
        feature_name=features,
    )

    model = lgb.train(
        params=params_lgb,
        train_set=tr_dataset,
        valid_sets=[tr_dataset, vl_dataset],
        num_boost_round=3000,
        feval=feval_pearsonr,
        callbacks=[
            lgb.early_stopping(stopping_rounds=300, verbose=True),
            lgb.log_evaluation(period=100),
        ],
    )

    # save model
    model.save_model(model_filepath)


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    train_model()
