"""model predict module: get predict"""
import click
import lightgbm as lgb
import pandas as pd


@click.command()
@click.argument("data_filepath", type=click.Path(exists=True))
@click.argument("model_filepath", type=click.Path())
@click.argument("pred_filepath", type=click.Path(), default=None)
def predict_model(data_filepath, model_filepath, pred_filepath=None):
    """
    Get predict using pre-trained model

    :param data_filepath: str or pathlib.Path, file path to feature data for predict
    :param model_filepath: str or pathlib.Path, file path to pre-trained and saved model
    :param pred_filepath: str, pathlib.Path or None, file path to the saving of predict.
    If it's None (by default) the prediction will not save

    :return: pd.DataFrame, result of predict
    """
    data = pd.read_csv(data_filepath, parse_dates=["Date"])

    # load of the model
    model = lgb.Booster(model_file=model_filepath)
    pred = model.predict(data)

    pred = pd.DataFrame(pred, columns=["predict"], index=data.index)

    if pred_filepath is not None:
        pred.to_csv(pred_filepath)

    return pred
