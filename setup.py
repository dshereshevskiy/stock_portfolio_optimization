from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Development of an algorithm and methodology for selecting stocks in a portfolio using Machine Learning algorithms',
    author='Dmitry Shereshevskiy',
    license='MIT',
)
