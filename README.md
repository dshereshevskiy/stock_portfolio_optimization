# Оптимизация фондового портфеля акций
stock_portfolio_optimization
==============================

Development of an algorithm and methodology for selecting stocks in a portfolio using Machine Learning algorithms

## Описание
- Проект направлен на создание моделей для оптимизации фондового портфеля
- Модели будут сравниваться с реальными доходами в будущем
- Портфель будет создаваться из акций, подходящих для прогнозов (около 2000 акций)
- Акции ранжируются от самой высокой до самой низкой 
ожидаемой доходности и оцениваются по разнице доходности между верхними и 
нижними 200 акциями
- Для обучения и тестирования модели будет доступ к финансовым данным с японского 
рынка, таким как информация об акциях и исторические цены на акции

## Оценки и метрики
1.	Фондовый портфель формируется и оптимизируется на основании рангов акций и оценивается по коэффициенту Шарпа дневной доходности спреда (см. описание Процедуры оценки ниже). 
2.	Для определения текущего ранга акций (см. описание Процедуры оценки ниже) используются прогнозы дневных доходностей акций (ML прогнозы). Метрика при оценке точности этих прогнозов следующая. Поскольку для выставления правильных рангов достаточно знать относительный прогноз доходности акций (относительно других акций, чтобы поставить на правильное место при ранжировании), то в качестве ML метрики прогноза достаточно использовать корреляцию предикта и таргета доходности (вместо, например, RMSE или MAE).

### Процедура оценки эффективности фондового портфеля, сформированного на базе ML прогнозов дневных доходностей акций:
Для этого нужно будет ранжировать каждую акцию, активную в данный день. Для ранжирования используются спрогнозированные доходности акций. Доходность за один день рассматривает 200 акций с наивысшим рейтингом (например, от 0 до 199) как купленные в портфель, а 200 акций с самым низким рейтингом (например, с 1999 по 1800) считаются вариантом дневного портфеля наихудшей доходности. Затем акции взвешиваются в зависимости от их рейтинга, и рассчитывается общая доходность портфеля, предполагая, что акции были куплены на следующий день (завтра) и проданы на следующий после этого день (т. е. послезавтра). При этом в соответствии с методикой расчета коэффициента Шарпа, для расчетов используется спред между портфелем максимальной (прогнозируемой по рангам) дневной доходности и портфелем минимальной прогнозируемой (по рангам) дневной доходности (т. е. премия к минимальной доходности, Rday).

Rday = Sup – Sdown,

SharpScore = Average(Rday) / STD(Rday),

где

- Rday – спред между максимальной (Sup) и минимальной (Sdown) дневной доходностью, 
- SharpScore – коэффициент Шарпа, рассчитанный за период управления портфелем на основании ежедневных значений Rday,
- Average(Rday) – матожидание Rday (среднедневное значение),
- STD(Rday) – стандартное отклонение Rday. 
	Таким образом, при оценке общей эффективности методики (коэффициента Шарпа) для расчета средней дневной доходности портфеля используются реальные доходности акций, а для расчета рангов акций и отбора акций в дневной портфель – как раз предсказанные доходности акций. 

#### Итого, формализация цели проекта в терминах машинного обучения:
•	разработка алгоритма и методики отбора акций в портфель через ML предикт рангов акций.



Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
