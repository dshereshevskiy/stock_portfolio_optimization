poetry add mlflow (pip install mlflow)

mlflow ui
(это пока локально)
    python mlflow_test.py
    mlflow server --backend-store-uri sqlite:///mlflow.db --default-artifact-root ./mlruns/artifacts/ --host 127.0.0.1

вариант на внешнем серваке
ставим portainer в поднятый удаленный сервак (у нас он ранее поднят на Я-клауд):
(инструкция здесь https://docs.portainer.io/start/install-ce/server/docker/linux)
    docker volume create portainer_data
    docker run -d -p 8000:8000 -p 9443:9443 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ee:latest
ставим docker-compose (на линукс он ставится отдельно)
    apt install docker-compose
копируем docker (с docker-compose.yml и .env)
    cd docker
    sudo docker-compose up -d --build
смотрим в браузере portainer
вписываем pgadmin в docker-compose, поднимаем  # это графический интерфейс (GUI) для базы postgres
смотрим в браузере <server-ip>:5050
входим по кредам (см. docker-compose)
добавляем minio в docker-compose, поднимаем  # это GUI для нашего собственного s3
входим по кредам (см. docker-compose и .env)

дополняем dvc.yaml и запускаем dvc repro
(модель билдится и кладется в mlflow сервер)

### забираем модель из mlflow сервера
локально
mlflow models serve --no-conda -m s3://arts/2/cb7361a23aba4fc890a323f96d1258ef/artifacts/model -h 0.0.0.0 -p 8001
если проблемы с кредами, может потребоваться запустить в консоли
export AWS_SECRET_ACCESS_KEY=minioadmin (или свой ключ)
или/и
export MLFLOW_S3_ENDPOINT_URL=http://158.160.106.86:9000 (или свой айпишник)
идем http://127.0.0.1:8001/ , но пока там пусто
делаем запрос через postman
{
    "inputs": {
        "High": [2755.0, 576.0, 3210.0],
        "Low": [2730.0, 563.0, 3140.0],
        "Open": [2734.0, 568.0, 3150.0],
        "Close": [2742.0, 571.0, 3210.0],
        "Volume": [31400, 2798500, 270800],
        "return_1month": [0.0, 0.0, 0.0],
        "return_2month": [0.0, 0.0, 0.0],
        "return_3month": [0.0, 0.0, 0.0],
        "volatility_1month": [0.0, 0.0, 0.0],
        "volatility_2month": [0.0, 0.0, 0.0],
        "volatility_3month": [0.0, 0.0, 0.0],
        "MA_gap_1month": [0.0, 0.0, 0.0],
        "MA_gap_2month": [0.0, 0.0, 0.0],
        "MA_gap_3month": [0.0, 0.0, 0.0]
    }
}
то же самое через докер ("lgbm" - имя контейнера)
mlflow models build-docker -m s3://arts/2/cb7361a23aba4fc890a323f96d1258ef/artifacts/model -n "lgbm"

в отдельном проекте-приложении
uvicorn inference:app --host 0.0.0.0 --port 8001
